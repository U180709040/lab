import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static boolean checkBoard(char[][] board, int rowLast, int colLast){
		char ctrl = board[rowLast -1][colLast -1];
		boolean flag = true;
		for(int row = 0;row < 3; row++){
			if(board[row][colLast - 1] != ctrl){
				flag = false;
				break;
			}
		}
		if(flag){
			return true;
		}
		flag = true;
		for (int col = 0;col < 3; col++){
			if(board[rowLast - 1][col] != ctrl){
				flag = false;
				break;
			}
		}
		if(flag)
			return true;

		flag = true;
		if (colLast == rowLast){
			for(int i = 0; i < 3; i++){
				if(board[i][i] != ctrl){
					flag = false;
					break;
				}
			}
			if(flag){
				return true;
			}
		}

		flag = true;
		if(colLast + rowLast == 4){
			int i, j;
			for(i = 0, j = 2; i < 3 && j > -1; i++, j--){
				if(board[i][j] != ctrl){
					flag = false;
					break;
				}
			}
			if(flag){
				return true;
			}
		}


		return false;
	}

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		int movecount = 0;
		int currentPlayer = 0;
		int row = 0, col = 0;

		while(movecount < 9){
			printBoard(board);
			do{
				System.out.print("Player "+ (currentPlayer + 1)+ " enter row number:");
				row = reader.nextInt();
				System.out.print("Player "+ (currentPlayer + 1)+ " enter column number:");
				col = reader.nextInt();
			}while(!(row > 0 && row< 4 && col > 0 && col < 4 && board[row - 1][col - 1] == ' '));
			board[row - 1][col - 1] = (currentPlayer == 0 ? 'X' : 'O');
			movecount++;

			if (checkBoard(board, row, col)){
				printBoard(board);
				System.out.println("Win Player "+ (currentPlayer + 1));
				break;
			}
			currentPlayer = (currentPlayer + 1) % 2;
		}
		reader.close();
		if(!checkBoard(board, row, col)){
			System.out.println("Draw");
		}
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}