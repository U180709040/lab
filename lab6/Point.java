public class Point {
    int xCoord=1;
    int yCoord=1;
    // Constructer
    public Point(int x , int y){
        xCoord=x;
        yCoord=y;
    }
    public Point(int xy){
        xCoord=xy;
        yCoord=xy;
    }
    public Point(){

    }
    public double distance(Point point){
        return Math.sqrt((xCoord- point.xCoord )*( xCoord- point.xCoord)+(yCoord- point.yCoord)*(yCoord- point.yCoord));
    }

}