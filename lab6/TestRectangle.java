public class TestRectangle {
    public static void main(String[] args) {
        Point p =new Point(3,7);
        Rectangle r =new Rectangle(5,6,p);
        Point p2 =new Point(3,10);
        Rectangle r2 =new Rectangle(7,9,p2);
        /*we can also do this
        Rectangle r =new Rectangle(5,6,new Rectangle(5,6,p));
         */
        System.out.println("area = "+ r.area());
        System.out.println("perimeter = "+ r.perimeter());
        System.out.println("area = "+ r2.area());
        System.out.println("perimeter = "+ r2.perimeter());

        Point[] corners = r.corners();
        for (int i=0; i<corners.length;  i++){
            System.out.println("x= " +corners[i].xCoord + " , y= " +corners[i].yCoord  );
        }
    }
}