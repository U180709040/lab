public class TestPoint {
    public static void main(String[] args) {
        Point p1 =new Point(3,2);
        System.out.println("x = "+p1.xCoord+" , y = "+p1.yCoord);
        p1.xCoord=3;
        p1.yCoord=2;
        Point p2=new Point(6);
        System.out.println("P2 x = "+p2.xCoord+" , y = "+p2.yCoord);
        Point p3 = new Point();
        System.out.println("P3 x = "+p3.xCoord+" , y = "+p3.yCoord);
        System.out.println("distance p1 to p2 = "+p1.distance(p2));
    }

}