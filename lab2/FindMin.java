public class FindMin {
    public static void main(String[] args){
        int result;
        int value1=Integer.parseInt(args[0]);
        int value2=Integer.parseInt(args[1]);
        int value3=Integer.parseInt(args[2]);
        boolean someConditions=value1 < value2;
        result= someConditions ? value1 : value2;
        boolean end=result<value3;
        result=end ? result : value3;
        System.out.println(result);

    }
}
