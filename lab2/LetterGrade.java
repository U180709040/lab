public class LetterGrade {
    public static void main(String[] args){
        int value = Integer.parseInt(args[0]);
        if (value<=100 && value>=90) {
            System.out.println("Your grade is A");
        } else if (value<90 && value>=80) {
            System.out.println("Your grade is B");
        } else if (value<80 && value>=70){
            System.out.println("Your grade is C");
        } else if (value<70 && value>=60){
            System.out.println("Your grade is D");
        }else if (value<60 && value>=0){
            System.out.println("Your grade is F");
        }else{
            System.out.println("Invalid score");
        }

    }
}
